class CompileError(Exception):
  """ All of the compiler errors extend this class.
  """
  pass


class LexerError(CompileError):
  """ Throwed if an error occured while lexing.  It will show the
  position of error.
  """
  def __init__(self, source, pos):
    self.line = source.count('\n', 0, pos) + 1
    self.column = pos - source.rfind('\n', 0, pos)

  def __str__(self):
    return 'at line {}, column {}.'.format(self.line, self.column)


