from collections import namedtuple
import re

from compiler.errors import *


Token = namedtuple('Token', ['code', 'attribute'])


def __token_gen__(finalrule, raw):
  """ Help function of generating token from final rule and raw lexeme.
  """
  attr = None if len(finalrule)<3 else finalrule[2](raw)
  return Token(finalrule[1], attr)

class Lexer(object):
  """ The base lexer.
  """
  def __init__(self, rules, skip_pattern=re.compile(r'\s+')):
    """ The skip_pattern should be compiled regular
    expression.  Rules are an iterable of Rule.  A rule is a tuple,
    and there are two kind of rule:
    (compiled re, token type number[, attribute generator]), or
    (compiled re, list of subrule)
    """
    self.__rules__ = rules
    self.__skip_pattern__ = skip_pattern

  def __tokens__(self, input, rules):
    pos = 0
    while pos<len(input):
      if self.__skip_pattern__:
        while True:
          match = self.__skip_pattern__.match(input, pos)
          if not match:
            break
          pos = match.end() 
          if pos == len(input):
            return
      for rule in rules:
        rule_match = rule[0].match(input, pos)
        if rule_match:
          pos = rule_match.end()
          if isinstance(rule[1], list):
            for t in self.__tokens__(rule_match.group(), rule[1]):
              yield t
          else:
            yield __token_gen__(rule, rule_match.group())
          break
      else:
        raise LexerError(input, pos) 

  def tokens(self, input):
    """ Lex the input.
    """
    for t in self.__tokens__(input, self.__rules__):
      yield t


