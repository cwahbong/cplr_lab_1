import sys

from compiler import cmm

def attr_formatter(attr):
  """ Return the formatted string of attribute.  The integer will be
  represented as hex.
  """
  return '{:#010x}'.format(attr) if isinstance(attr, int) else attr


def __main__():
  """ Lex the content of the first file and print the token to the
  output file.  The first argument is the input file and the second
  argument is the output file.
  """
  input_file = open(sys.argv[1], 'r')
  output_file = open(sys.argv[2], 'w')
  for token in cmm.lexer.tokens(''.join(input_file)):
    if token[1] is None:
      output_file.write('{}\n'.format(token[0]))
    else:
      output_file.write('{}\t{}\n'.format(token[0], attr_formatter(token[1])))
  input_file.close()
  output_file.close()

if __name__=='__main__':
  __main__()

